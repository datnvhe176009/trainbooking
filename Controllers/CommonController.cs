﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PRN211_Project_Group_4.Models;

namespace G4.Controllers
{
    public class CommonController : Controller
    {
        private readonly PRN211Context _context;

        public CommonController(PRN211Context context)
        {
            _context = context;
        }


        // GET: CommonController
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BookSeat()
        {

            int wagonId = 2;
            var wagon = _context.Wagons
                .Include(w => w.Train)
                .FirstOrDefault(w => w.WagonId == wagonId);
            var train = wagon.Train;
            var trips = _context.Trips
                .Where(trip => trip.TrainId == train.TrainId).ToList();
            return View();
        }

        public ActionResult MyTicket()
        {
            return View();
        }

        public ActionResult TicketDetail()
        {
            return View();
        }


        // GET: CommonController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CommonController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CommonController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CommonController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CommonController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CommonController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CommonController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
