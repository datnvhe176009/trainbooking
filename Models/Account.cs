﻿using System;
using System.Collections.Generic;

namespace PRN211_Project_Group_4.Models
{
    public partial class Account
    {
        public Account()
        {
            Bookings = new HashSet<Booking>();
        }

        public int AccountId { get; set; }
        public string? Username { get; set; }
        public string? Password { get; set; }
        public int? Role { get; set; }

        public virtual ICollection<Booking> Bookings { get; set; }
    }
}
